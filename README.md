## MakerJS Live Reload Server

![alt text](.livePreview/img/livePreviewScreenshot.png "")
### Install

```
npm i
```

### Usage

```
npm start
```
A browser tab showing the output svg will open.

When a component is saved: 
- the new svg will be generated
- the openscad preview will update

The browser will update automatically to show the latest generated svg.

