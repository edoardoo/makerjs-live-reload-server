exports.BASE_SIZE = 200;
exports.BASE_RADIUS = exports.BASE_SIZE/(16.7/3.14);
exports.HOLE_SIZE = 2;
exports.HOLE_DISTANCE = 2;
exports.MARGIN = 5;