const bs = require("browser-sync").create();
const { exec } = require("child_process");
const  fs = require("fs");

bs.init({
    server: `${__dirname}/../`,
    startPath: 'index.html'
});

bs.watch('../**/*.js').on('change', async(fileName)=>{
    await draw();
    bs.reload();
});

bs.watch('../**/*.svg').on('change', async(fileName)=>{
    await composePreview(fileName);
    bs.reload();
});

function getFile(path){
    return new Promise((resolve, reject)=>{
        
        fs.readFile(path, 'utf8' , (err, data) => {
            if (err) {
                console.error(err)
                reject();
            }
            resolve(data);
        })
    })
}

function composePreview(fileName){
    console.log(fileName);
    Promise.all([ 
        getFile(`${__dirname}/index-template.html`),
        getFile(`${__dirname}/../${fileName}`)
    ]).then((result)=>{
        const indexWithSVG = result[0].replace("#{svgOutput}",result[1]);
        fs.writeFileSync(`${__dirname}/../index.html`, indexWithSVG);
    }).catch(e=>{
        console.log(e);
    })
}

function draw(){
    return exec("node index.js", (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log(stdout);
    });
}
draw();