const fs = require('fs');
const makerjs = require('makerjs');
const { exec } = require("child_process");

const { 
    BASE_SIZE,
    BASE_RADIUS,
    HOLE_SIZE,
    HOLE_DISTANCE,
    MARGIN
 } = require('../../consts.js');

const componentName = 'base';
console.log(`Generating ${componentName}`);

let outer = new makerjs.models.RoundRectangle(BASE_SIZE, BASE_SIZE, BASE_RADIUS);

//TODO: there's a better way to do this:
let patternSize = BASE_SIZE-2*MARGIN;
let distanceFromBorder = -(BASE_SIZE-patternSize)/2;
makerjs.model.move(outer, [distanceFromBorder,distanceFromBorder]);

const rows = columns = Math.round((BASE_SIZE-2*MARGIN)/(HOLE_SIZE+HOLE_DISTANCE));
let pattern = [];
for (let i = 0; i < rows; i++) {
    for (let j = 0; j < columns; j++) {
        let square =  new makerjs.models.Rectangle(HOLE_SIZE,HOLE_SIZE)
        square = makerjs.model.move(square, [ i*(HOLE_SIZE+HOLE_DISTANCE), j*(HOLE_SIZE+HOLE_DISTANCE)] );
        pattern.push(square);
    }    
}


const completeobject = {
    paths: {
    },
    models: [
        outer,
        ...pattern
    ]
}
let svg = makerjs.exporter.toSVG(completeobject, {units:makerjs.unitType.Millimeter});
fs.writeFile(`./svg/${componentName}.svg`, svg, () => { });
return exec(`touch ${__dirname}/base.scad`, (error, stdout, stderr) => {});

module.exports = svg;
